-- Which staff members made the highest revenue for each store and deserve a bonus for the year 2017?

WITH
    StaffRevenue
    AS
    (
        SELECT
            s.store_id,
            st.staff_id,
            st.first_name,
            st.last_name,
            SUM(p.amount) AS total_revenue,
            ROW_NUMBER() OVER (PARTITION BY s.store_id ORDER BY SUM(p.amount) DESC) AS rank
        FROM
            store s
            JOIN
            staff st ON s.store_id = st.store_id
            JOIN
            rental r ON st.staff_id = r.staff_id
            JOIN
            payment p ON r.rental_id = p.rental_id
        WHERE
        EXTRACT(YEAR FROM p.payment_date) = 2017
        GROUP BY
        s.store_id, st.staff_id, st.first_name, st.last_name
    )
SELECT
    store_id,
    staff_id,
    first_name,
    last_name,
    total_revenue
FROM
    StaffRevenue
WHERE
    rank = 1;

-- Which five movies were rented more than the others, and what is the expected age of the audience for these movies?

SELECT
    f.film_id,
    f.title,
    COUNT(r.rental_id) AS rental_count
FROM
    film f
    JOIN
    inventory i ON f.film_id = i.film_id
    JOIN
    rental r ON i.inventory_id = r.inventory_id
GROUP BY
    f.film_id, f.title
ORDER BY
    rental_count DESC
LIMIT 5;

-- Which actors/actresses didn't act for a longer period of time than the others?

WITH FilmLastRental
AS
(
    SELECT
    i.film_id,
    MAX(r.rental_date) AS last_rental_date
FROM
    inventory i
    JOIN
    rental r ON i.inventory_id = r.inventory_id
GROUP BY
        i.film_id
)
,


ActorLastRental AS
(
    SELECT
    a.actor_id,
    a.first_name,
    a.last_name,
    MAX(flr.last_rental_date) AS last_acting_date
FROM
    actor a
    JOIN
    film_actor fa ON a.actor_id = fa.actor_id
    JOIN
    FilmLastRental flr ON fa.film_id = flr.film_id
GROUP BY
        a.actor_id, a.first_name, a.last_name
)


SELECT
    alr.actor_id,
    alr.first_name,
    alr.last_name,
    alr.last_acting_date,
    CURRENT_DATE - alr.last_acting_date AS days_inactive
FROM
    ActorLastRental alr
ORDER BY
    days_inactive DESC
LIMIT 5;
